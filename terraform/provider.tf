terraform {
  required_version = ">=1.0"

  required_providers {
    azapi = {
      source  = "azure/azapi"
      version = "~>1.5"
    }
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~>3.0"
    }
    random = {
      source  = "hashicorp/random"
      version = "~>3.0"
    }
    time = {
      source  = "hashicorp/time"
      version = "0.9.1"
    }
  }
}

provider "azurerm" {
  skip_provider_registration   = true
  features {}

  /*subscription_id   = "9dfc6d28-aa28-46ab-8517-c6a0b737f924"
  tenant_id         = "b7b023b8-7c32-4c02-92a6-c8cdaa1d189c"
  client_id         = "6cbf7e37-6f02-410c-86a2-7e97b17d6168"
  client_secret     = "TMd8Q~lj55FZx6YsDVhoNPIYAJGI4VOFJaxn.c7R"*/
}