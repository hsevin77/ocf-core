# Projet CI/CD OCF Core
- Robin GUERINET
- Jimmy HAGGEGE
- Hugo SEVIN
- Guillaume LIEUGAUT

## Partie 1: CI/CD avec GitLab et Kubernetes

### Création du cluster local K3D

Utilisation du projet GitHub : https://github.com/k3d-io/k3d-demo

#### Prérequis :

```
apt update -y
```

- curl, wget et sudo

```
apt install curl wget sudo -y
```

- Docker

https://docs.docker.com/engine/install/

- K3D

```
wget -q -O - https://raw.githubusercontent.com/k3d-io/k3d/main/install.sh | bash
```

- kubectl

https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/

```
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
```

```
sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
```

#### Création d'un cluster

Configurer un cluster local avec K3D est un processus simple pour expérimenter avec Kubernetes en environnement de développement.

**Pour le déploiement du cluster :**

Exécuter la commande :

```
k3d cluster create "nom_cluster" --api-port 6550 --servers 1 --agents 2 --port 8080:80@loadbalancer --wait
```

### Helm

Helm est intégré dans le projet pour simplifier le déploiement d'applications sur le cluster Kubernetes en utilisant des packages (charts) préconfigurés, facilitant ainsi la gestion, la mise à jour, et le partage des configurations d'application.

#### Installation

1. Installation de Helm

```
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
```

2. Ajout droit d'exécution au script

```
chmod 700 get_helm.sh
```

3. Exécution du script

```
./get_helm.sh
```

![helm](images/helm.png)

#### Création des charts

Commande :

```
helm create "nom_chart"
```

Cela va créer l'architecture automatiquement, ensuite il faudra modifier value.yaml et deployment.yaml

#### Configuration

Helm utilise les confs de Kubectl.

1. Mettre les manifest Kubernetes dans le fichier template (à la racine de la chart)

![helm1](images/helm1.png)

2. Lancer la chart avec la commande suivante :

```
helm install "nom_chart" "chemin_dossier_chart"
```

### Terraform

**Initialisation :**

Accédez au répertoire contenant les fichiers Terraform et exécuter la commande d'initialisation pour télécharger les modules et les plugins nécessaires.

```
terraform init
```

**Vérification du Plan :**

Exécuter la commande plan pour voir quels changements Terraform prévoit d'apporter à l'infrastructure.

A utiliser seulement en local (sans pipeline)

```
terraform plan
```

**Application des Changements :**

Si le plan semble correct, exécuter la commande apply pour appliquer les changements proposés.

```
terraform apply -auto-approve
```

### Pipeline CI/CD

**Création agent GitLab pour Kubernetes**

Actions à faire dans la branche main

Création du répertoire :

Pour le dev :
```
mkdir -p .gitlab/agents/kubeagent
```

```
cd /gitlab/agents/kubeagent
```

```
touch config.yaml
```
------------------------------

Pour Azure :

```
cd .gitlab/agents/
mkdir kubeagentazure
```

```
touch config.yaml
```

**Explication des étapes du pipeline CI/CD dans le main**

Un autre pipeline est présent dans la branche develop mais ne comporte pas l'étape 3. Terraform

1. Initialisation des variables

Les variables telles que $mailazure, $passwdazure, $tokenazure, et $subscription sont initialisées pour stocker des informations sensibles et des paramètres spécifiques à l'environnement.

2. Build (build)

Cette étape construit une image Docker à l'aide du fichier Dockerfile situé dans le répertoire images/az-cli-kubectl-helm/. L'image contient des outils tels que Azure CLI, kubectl et Helm. Elle est ensuite poussée vers un registre Docker privé GitLab.

3. Lancement Terraform (infra_azure)

La deuxième étape utilise l'image Docker Zenika. Elle se connecte à Azure avec les identifiants spécifiés, puis applique la configuration Terraform pour créer et gérer l'infrastructure nécessaire sur Azure. L'option -auto-approve permet d'automatiser le processus sans confirmation manuelle.

4. Déploiement Helm (helm_deploy_agentkube)

La dernière étape utilise l'image Docker construite dans l'étape build. Elle se connecte à Azure, récupère les informations d'authentification pour le cluster AKS en se connectant à celui-ci et utilise Helm pour déployer l'application sur le cluster. Le déploiement utilise le chart Helm spécifié dans le répertoire helm/deploy/.

**Liaison agent Gitlab avec Kubernetes**

Exécution des commandes :

```
helm repo add gitlab https://charts.gitlab.io/
helm repo update
helm upgrade --install kubeagentazure gitlab/gitlab-agent \--namespace gitlab-agent-kubeagentazure \--create-namespace \--set image.tag=v16.7.0-rc3 \--set config.token=$tokenazure \--set config.kasAddress=wss://kas.gitlab.com
helm upgrade --install deploy helm/deploy/
```

**Test de l'application en load balancing**

Configuration fichier service_golang.yml :

Ce fichier YAML décrit un service dans Kubernetes nommé my-golang-service. Ce service permet de rediriger le trafic vers des pods spécifiques.

Il expose l'application à l'extérieur du cluster sur le port 80 à l'aide d'un load load balancer. Ainsi, les utilisateurs externes peuvent accéder à l'application via l'IP publique associée à l'équilibreur de charge.

Récupérer l'IP publique en tapant la commande :

```
kubectl get services
```

Puis entrer la valeur "EXTERNAL-IP" dans un navigateur pour se connecter.

![external_ip](images/external_ip.png)

**Test de l'application en node exporter (local uniquement)**

Installe Azure CLI sur le système :

```
apt install azure-cli -y
```

Permet de se connecter à Azure :

```
az login -u "mail_azure" -p "mdp"
```

Définit l'abonnement Azure à utiliser :

```
az account set --subscription "azure_tenant_id"
```

Configure kubectl pour utiliser les informations d'authentification du cluster AKS spécifié :

```
az aks get-credentials --resource-group azure-projet --name my-k8s-cluster
```

Affiche la liste des nœuds dans le cluster Kubernetes :

```
kubectl get nodes
```

Affiche la liste des services Kubernetes dans le cluster :

```
kubectl get services
```

Met en place une redirection de port, permettant d'accéder localement au service "my-golang-service" du cluster Kubernetes sur le port spécifié :

```
kubectl port-forward service/my-golang-service "port":80
```

Dans un autre terminal, exécuter cette commande :

```
curl http://localhost:"port"/swagger/index.html
```

### Accès à l'application

Cette commande Kubernetes permet de rediriger localement le port du service vers le port 80 dans le cluster.

```
kubectl get services
```

```
kubectl port-forward service/"nom_application" "port-application":80
```

Accès via l'URL : [http://localhost:"port"/swagger/index.html](url)

![swagger](images/swagger.png)

## Partie 2: CI/CD avec ArgoCD

### Installation de ArgoCD

S'assurer d'avoir la dernière version de K3D :
```
k3d version
```

![k3d_version](images/k3d_version.png)

**Mise à jour du repository avec Helm :**

```
helm repo add argo-cd https://argoproj.github.io/argo-helm
```

```
helm repo update
```

Créer un fichier nommé values.yaml qui contient :

```
server:
  ingress:
    enabled: true
```

**Création du namespace argocd :**

```
kubectl create namespace argocd
```

**Installation de ArgoCD via Helm et values.yaml :**

```
helm install argocd argo-cd/argo-cd -n argocd -f values.yaml
```

### Configuration ArgoCD

Récupération du mdp admin généré par ArgoCD :
```
kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d
```

Création du rôle du cluster :
```
kubectl create clusterrolebinding argocd-cluster-admin-binding --clusterrole=cluster-admin --serviceaccount=argocd:argocd-service-account
```

Création du compte de service :
```
kubectl create serviceaccount argocd-service-account -n argocd
```

**Installation de la CLI ARGOCD :**

```
curl -sSL -o /usr/local/bin/argocd https://github.com/argoproj/argo-cd/releases/latest/download/argocd-linux-amd64
```

Donner les droits d'exécution et pouvoir l'appeler en bash :
```
chmod +x /usr/local/bin/argocd
```

Sur un autre terminal, faire la redirection de port :
```
kubectl port-forward svc/argocd-server -n argocd 80:443
```

Se connecter via la CLI ArgoCD :
```
argocd login localhost:80 --username=admin --password=xyLZFEGV-HkrFV69 (screen dans le chat)
```

Créer un projet ArgoCD :
```
argocd proj create my-app-golang
```

### Définition des applications ArgoCD

**Definition d'application :**

Créer un fichier nommé app_argocd.yaml qui contient :
```
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: my-app
  namespace: argocd
spec:
  destination:
    server: k3d-mycluster
    namespace: default
  source:
    repoURL: https://gitlab.com/jimmyhaggege/ocf-core.git
    path: /helm/deploy/
    targetRevision: main
  project: my-project
  syncPolicy:
    automated:
      prune: true
      selfHeal: true
```

Exécution du fichier app_argocd.yaml :
```
kubectl apply -f app_argocd.yaml
```

Afficher l'application en CLI
```
argocd app get my-app
```

Synchronisation de l'application avec GitLab :
```
argocd app sync my-app
```

**Connexion à l'interface web ArgoCD :**

Via [http://ip-argocd:port-argocd](url)

![argocd1](images/argocd1.png)

## Annexes

### Documentation des outils

- https://helm.sh/fr/docs/intro/quickstart/
- https://k3d.io/
- https://argoproj.github.io/cd/
- https://www.terraform.io/
- https://kubernetes.io/fr/
